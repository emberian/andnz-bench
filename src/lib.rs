#![feature(stdsimd)]
use std::simd::*;

#[inline(never)]
pub fn serial_and32(s1: &[u32], s2: &[u32]) -> bool {
	for (w1, w2) in s1.iter().cloned().zip(s2.iter().cloned()) {
		if w1 & w2 != 0 {
			return true;
		}
	}
	false
}

#[inline(never)]
pub fn serial_and64(s1: &[u64], s2: &[u64]) -> bool {
	for (w1, w2) in s1.iter().cloned().zip(s2.iter().cloned()) {
		if w1 & w2 != 0 {
			return true;
		}
	}
	false
}

#[inline(never)]
pub fn simd_and64x2(s1: &[u64], s2: &[u64]) -> bool {
	for (w1, w2) in s1.iter().cloned().zip(s2.iter().cloned()) {
		if u64x2::new(w1, w2).and() != 0 {
			return true;
		}
	}
	false
}

#[inline(never)]
pub fn simd_and32x4(s1: &[u32], s2: &[u32]) -> bool {
	// assume the slices are aligned and of the right length
	let zero = u32x4::splat(0);
	for i in 0..s1.len() / 4 {
		let v1 = unsafe {
			u32x4::load_aligned_unchecked(&s1[i*4..])
		};
		let v2 = unsafe {
			u32x4::load_aligned_unchecked(&s2[i*4..])
		};
		if v1 & v2 != zero {
			return true;
		}
	}
	false
}

#[inline(never)]
pub fn simd_and64x4(s1: &[u64], s2: &[u64]) -> bool {
	// assume the slices are aligned and of the right length
	let zero = u64x4::splat(0);
	for i in 0..s1.len() / 4 {
		let v1 = unsafe {
			u64x4::load_aligned_unchecked(&s1[i*4..])
		};
		let v2 = unsafe {
			u64x4::load_aligned_unchecked(&s2[i*4..])
		};
		if v1 & v2 != zero {
			return true;
		}
	}
	false
}

#[inline(never)]
pub fn simd_and32x8(s1: &[u32], s2: &[u32]) -> bool {
	// assume the slices are aligned and of the right length
	let zero = u32x8::splat(0);
	for i in 0..s1.len() / 8 {
		let v1 = unsafe {
			u32x8::load_aligned_unchecked(&s1[i*8..])
		};
		let v2 = unsafe {
			u32x8::load_aligned_unchecked(&s2[i*8..])
		};
		if v1 & v2 != zero {
			return true;
		}
	}
	false
}
