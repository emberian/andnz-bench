#![feature(vec_resize_default)]
extern crate andnz_bench;
#[macro_use]
extern crate criterion;

use criterion::Criterion;
use andnz_bench::*;

#[repr(align(16))]
#[derive(Default)]
struct E8<T>([T; 8]);

fn to_u32(v: &Vec<E8<u32>>) -> &[u32] {
	unsafe {
		std::slice::from_raw_parts(v.as_ptr() as *const _, v.len()*8)
	}
}

fn to_u64(v: &Vec<E8<u32>>) -> &[u64] {
	unsafe {
		std::slice::from_raw_parts(v.as_ptr() as *const _, v.len()*4)
	}
}

fn bench_all(c: &mut Criterion) {
	use criterion::Fun;

	let mut many_zeros = Vec::<E8<u32>>::with_capacity(64);
	many_zeros.resize_default(1024);
	many_zeros[255].0[7] = 1;
	
	let funcs32 = vec![
		Fun::new("serial_and32", |b, &i| b.iter(|| { serial_and32(i, i); })),
		Fun::new("simd_and32x4", |b, &i| b.iter(|| { simd_and32x4(i, i); })),
		Fun::new("simd_and32x8", |b, &i| b.iter(|| { simd_and32x8(i, i); })),
	];
	let funcs64 = vec![
		Fun::new("serial_and64", |b, &i| b.iter(|| { serial_and64(i, i); })),
		Fun::new("simd_and64x2", |b, &i| b.iter(|| { simd_and64x2(i, i); })),
		Fun::new("simd_and64x4", |b, &i| b.iter(|| { simd_and64x4(i, i); })),
	];

	// i apologize for my sins; i have to leave for work soon
	c.bench_functions("andnz64", funcs64, &to_u64(unsafe { std::mem::transmute::<&Vec<_>, &'static Vec<_>>(&many_zeros) }));
	c.bench_functions("andnz32", funcs32, &to_u32(unsafe { std::mem::transmute::<&Vec<_>, &'static Vec<_>>(&many_zeros) }));

	std::mem::forget(many_zeros); // leak so the pointers never become invalid
}

criterion_group!(benches, bench_all);
criterion_main!(benches);
